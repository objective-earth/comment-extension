# Comment Extension



## About

This project is a fork of https://www.mediawiki.org/wiki/Extension:Comments

## Installation

- Download and place the file(s) in a directory called Comments in your extensions/ folder. Add the following code at the bottom of your LocalSettings.php:

   ```wfLoadExtension( 'comment-extension' );```

-  Run the [update](https://www.mediawiki.org/wiki/Manual:Update.php) script which will automatically create the necessary database tables that this extension needs.
- Navigate to Special:Version on your wiki to verify that the extension is successfully installed.




